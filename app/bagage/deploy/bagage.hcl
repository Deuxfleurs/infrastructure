job "bagage" {
  datacenters = ["dc1"]
  type = "service"
  priority = 90

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "main" {
    count = 1

    network {
      port "web_port" { to = 8080 }
      port "ssh_port" { 
        static = 2222
        to = 2222
      }
    }

    task "server" {
      driver = "docker"
      config {
        image = "superboum/amd64_bagage:v11"
        readonly_rootfs = false
        volumes = [
          "secrets/id_rsa:/id_rsa"
        ]
        ports = [ "web_port", "ssh_port" ]
      }

      env {
        BAGAGE_LDAP_ENDPOINT = "bottin2.service.2.cluster.deuxfleurs.fr:389"
      }

      resources {
        memory = 500
      }

      template {
        data = "{{ key \"secrets/bagage/id_rsa\" }}"
        destination = "secrets/id_rsa"
      }

      service {
        name = "bagage-ssh"
        port = "ssh_port"
        address_mode = "host"
        tags = [
          "bagage",
          "(diplonat (tcp_port 2222))"
        ]
      }

      service {
        name = "bagage-webdav"
        tags = [
          "bagage",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:bagage.deuxfleurs.fr",
          "tricot bagage.deuxfleurs.fr",
        ]
        port = "web_port"
        address_mode = "host"
        check {
          type = "tcp"
          port = "web_port"
          address_mode = "host"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

