bind = yes
bind_dn = {{ key "secrets/email/postfix/ldap_binddn" | trimSpace }}
bind_pw = {{ key "secrets/email/postfix/ldap_bindpwd" | trimSpace }}
version = 3
timeout = 20
start_tls = no
tls_require_cert = no
server_host = ldap://bottin2.service.2.cluster.deuxfleurs.fr
scope = sub
search_base = ou=users,dc=deuxfleurs,dc=fr
query_filter = mail=%s
result_attribute = mail
