job "platoo" {
  datacenters = ["dc1"]
  type = "service"
  priority = 10

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "core" {
    network {
      port "web_port" { to = 8080 }
    }

    task "nodejs" {
      driver = "docker"
      config {
        image = "victormoi/platoo:v1"
        force_pull = true
        ports = [ "web_port" ]
      }

      template {
        data = <<EOH
user=platoo
host=psql-proxy.service.2.cluster.deuxfleurs.fr
database=platoodb
password={{ key "secrets/platoo/bddpw" | trimSpace }}
EOH
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 400
      }

      service {
        tags = [
          "platoo",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https",
          "traefik.frontend.rule=Host:platoo.deuxfleurs.fr;PathPrefix:/",
          "tricot platoo.deuxfleurs.fr",
        ]
        port = "web_port"
        address_mode = "host"
        name = "platoo"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

