{
  pkgs ? import <nixpkgs> {}
}:

with pkgs; mkShell {
  nativeBuildInputs = [
    nomad 
    docker-compose
    python39Packages.pip 
    python39Packages.ldap 
    python39Packages.consul 
    python39Packages.passlib
  ];
}

