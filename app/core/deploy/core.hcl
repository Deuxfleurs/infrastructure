job "core" {
  datacenters = ["dc1", "neptune"]
  type = "system"
  priority = 90

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  update {
    max_parallel     = 1
    stagger = "1m"
  }

  group "network" {
    task "diplonat" {
      driver = "docker"

      config {
        image = "lxpz/amd64_diplonat:3"
        network_mode = "host"
        readonly_rootfs = true
        privileged = true
      }

      restart {
        interval = "30m"
        attempts = 2
        delay    = "15s"
        mode     = "delay"
      }

      template {
        data = <<EOH
DIPLONAT_REFRESH_TIME=60
DIPLONAT_EXPIRATION_TIME=300
DIPLONAT_CONSUL_NODE_NAME={{ env "attr.unique.hostname" }}
RUST_LOG=debug
EOH
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 40
      }
    }
  }
}
