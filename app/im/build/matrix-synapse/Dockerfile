FROM amd64/debian:buster as builder

ARG VERSION
ARG S3_VERSION
RUN apt-get update && \
    apt-get -qq -y full-upgrade && \
    apt-get install -y \
      python3 \
      python3-pip \
      python3-dev \
      python3-setuptools \
      libffi-dev \
      build-essential \
      libssl-dev \
      libjpeg-dev \
      libjpeg62-turbo-dev \
      libxml2-dev \
      zlib1g-dev \
      # postgresql-dev \
      libpq-dev \
      virtualenv \
      libxslt1-dev \
      git && \
    virtualenv /root/matrix-env -p /usr/bin/python3 && \
    . /root/matrix-env/bin/activate && \
    pip3 install \
      https://github.com/matrix-org/synapse/archive/v${VERSION}.tar.gz#egg=matrix-synapse[matrix-synapse-ldap3,postgres,resources.consent,saml2,url_preview] && \
    pip3 install \
      git+https://github.com/matrix-org/synapse-s3-storage-provider.git@${S3_VERSION}

FROM amd64/debian:buster

RUN apt-get update && \
    apt-get -qq -y full-upgrade && \
    apt-get install -y \
      python3 \
      python3-distutils \
      libffi6 \
      libjpeg62-turbo \
      libssl1.1 \
      libxslt1.1 \
      libpq5 \
      zlib1g \
      libjemalloc2 \
      ca-certificates

ENV LD_PRELOAD /usr/lib/x86_64-linux-gnu/libjemalloc.so.2
COPY --from=builder /root/matrix-env /root/matrix-env
COPY matrix-s3-async /usr/local/bin/matrix-s3-async
COPY entrypoint.sh /usr/local/bin/entrypoint

ENTRYPOINT ["/usr/local/bin/entrypoint"]
