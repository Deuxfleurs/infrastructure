job "plume-blog" {
  datacenters = ["dc1"]
  type = "service"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "plume" {
    count = 1

    network {
      port "web_port" { }
    }

    task "plume" {
      constraint {
        attribute = "${attr.unique.hostname}"
        operator = "="
        value = "digitale"
      }

      driver = "docker"
      config {
        image = "superboum/plume:v8"
        network_mode = "host"
        ports = [ "web_port" ]
        #command = "cat"
        #args = [ "/dev/stdout" ]
        volumes = [
          "/mnt/ssd/plume/search_index:/app/search_index",
          "/mnt/ssd/plume/media:/app/static/media"
        ]
      }

      template {
        data = file("../config/app.env")
        destination = "secrets/app.env"
        env = true
      }

      resources {
        memory = 500
        cpu = 100
      }

      service {
        name = "plume"
        tags = [
          "plume",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:plume.deuxfleurs.fr",
          "tricot plume.deuxfleurs.fr",
        ]
        port = "web_port"
        address_mode = "host"
        check {
          type = "http"
          protocol = "http"
          port = "web_port"
          path = "/"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "600s"
            ignore_warnings = false
          }
        }
      }
      restart {
        interval = "30m"
        attempts = 20
        delay    = "15s"
        mode     = "delay"
      }
    }
  }
}

