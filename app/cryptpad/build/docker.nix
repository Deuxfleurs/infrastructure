let
  common = import ./common.nix;
  pkgs = import common.pkgsSrc {};
in
  pkgs.dockerTools.buildImage {
    name = "superboum/cryptpad";
    config = {
      Cmd = [ "${pkgs.cryptpad}/bin/cryptpad" ];
    };
 }
