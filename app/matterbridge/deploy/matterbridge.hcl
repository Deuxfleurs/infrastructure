job "matterbridge" {
  datacenters = ["dc1"]
  type = "service"
  priority = 90

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "main" {
    count = 1

    task "bridge" {
      driver = "docker"
      config {
        image = "42wim/matterbridge:1.23"
        readonly_rootfs = true
        volumes = [
          "secrets/matterbridge.toml:/etc/matterbridge/matterbridge.toml"
        ]
      }

      resources {
        memory = 200
      }

      template {
        data = file("../config/matterbridge.toml")
        destination = "secrets/matterbridge.toml"
      }

	  restart {
	    attempts = 10
		delay = "30s"
	  }
	}
  }
}

