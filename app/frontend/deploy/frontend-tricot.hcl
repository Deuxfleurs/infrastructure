job "frontend" {
  datacenters = ["dc1", "neptune"]
  type = "service"
  priority = 90

  group "tricot" {
    network {
      port "http_port" { static = 80 }
      port "https_port" { static = 443 }
    }

    task "server" {
      driver = "docker"

      config {
        image = "lxpz/amd64_tricot:37"
        network_mode = "host"
        readonly_rootfs = true
        ports = [ "http_port", "https_port" ]
      }

      resources {
        cpu = 2000
        memory = 500
      }

      restart {
        interval = "30m"
        attempts = 2
        delay    = "15s"
        mode     = "delay"
      }

      template {
        data = <<EOH
TRICOT_NODE_NAME={{ env "attr.unique.hostname" }}
TRICOT_LETSENCRYPT_EMAIL=alex@adnab.me
TRICOT_ENABLE_COMPRESSION=true
RUST_LOG=tricot=debug
EOH
        destination = "secrets/env"
        env = true
      }

      service {
        name = "tricot-http"
        port = "http_port"
        tags = [ "(diplonat (tcp_port 80))" ]
        address_mode = "host"
      }

      service {
        name = "tricot-https"
        port = "https_port"
        tags = [ "(diplonat (tcp_port 443))" ]
        address_mode = "host"
      }
    }
  }
}
