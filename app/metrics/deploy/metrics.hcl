job "metrics" {
	datacenters = ["dc1"]
	type = "system"
	priority = "100"

	group "node_exporter" {
		task "node_exporter" {
			driver = "docker"

			config {
				image = "quay.io/prometheus/node-exporter:v1.1.2"
				network_mode = "host"
				volumes = [
					"/:/host:ro,rslave"
				]
				args = [ "--path.rootfs=/host" ]
			}

			resources {
				cpu = 50
				memory = 40
			}
		}
	}
}
