job "garage" {
  datacenters = ["dc1", "saturne", "neptune"]
  type = "system"
  priority = 80

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "garage" {
    network {
      port "s3" { static = 3900 }
      port "rpc" { static = 3901 }
      port "web" { static = 3902 }
    }

	update {
		max_parallel = 1
		min_healthy_time = "30s"
		healthy_deadline = "5m"
	}

    task "server" {
      driver = "docker"
      config {
        advertise_ipv6_address = true
        image = "dxflrs/amd64_garage:v0.7.1"
        command = "/garage"
        args = [ "server" ]
        network_mode = "host"
        volumes = [
          "/mnt/storage/garage/data:/data",
          "/mnt/ssd/garage/meta:/meta",
          "secrets/garage.toml:/etc/garage.toml",
        ]
        logging {
          type = "journald"
        }
      }

      template {
        data = file("../config/garage.toml")
        destination = "secrets/garage.toml"
      }

      resources {
        memory = 1500
        cpu = 1000
      }

      kill_signal = "SIGINT"
      kill_timeout = "20s"

      service {
        tags = [
          "garage_api",
          "tricot garage.deuxfleurs.fr",
          "tricot *.garage.deuxfleurs.fr",
        ]
        port = 3900
        address_mode = "driver"
        name = "garage-api"
        check {
          type = "tcp"
          port = 3900
          address_mode = "driver"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        tags = ["garage-rpc"]
        port = 3901
        address_mode = "driver"
        name = "garage-rpc"
        check {
          type = "tcp"
          port = 3901
          address_mode = "driver"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        tags = [
            "garage-web",
            "tricot * 1",
            "tricot-add-header Content-Security-Policy default-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline' https://code.jquery.com/; frame-ancestors 'self'",
            "tricot-add-header Strict-Transport-Security max-age=63072000; includeSubDomains; preload",
            "tricot-add-header X-Frame-Options SAMEORIGIN",
            "tricot-add-header X-XSS-Protection 1; mode=block",
        ]
        port = 3902
        address_mode = "driver"
        name = "garage-web"
        check {
          type = "tcp"
          port = 3902
          address_mode = "driver"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      restart { 
        interval = "30m"  
        attempts = 10  
        delay    = "15s"  
        mode     = "delay"
      }
    }
  }
}
