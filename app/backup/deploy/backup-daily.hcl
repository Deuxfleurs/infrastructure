job "backup_daily" {
  datacenters = ["dc1"]
  type = "batch"

  priority = "60"

  periodic {
    cron = "@daily"
    // Do not allow overlapping runs.
    prohibit_overlap = true
  }

  group "backup-dovecot" {
    constraint {
      attribute = "${attr.unique.hostname}"
      operator = "="
      value = "digitale"
    }

    task "main" {
      driver = "docker"

      config {
        image = "restic/restic:0.12.1"
        entrypoint = [ "/bin/sh", "-c" ]
        args = [ "restic backup /mail && restic forget --keep-within 1m1d --keep-within-weekly 3m --keep-within-monthly 1y && restic prune --max-unused 50% --max-repack-size 2G && restic check" ]
        volumes = [
          "/mnt/ssd/mail:/mail"
        ]
      }

      template {
        data = <<EOH
AWS_ACCESS_KEY_ID={{ key "secrets/email/dovecot/backup_aws_access_key_id" }}
AWS_SECRET_ACCESS_KEY={{ key "secrets/email/dovecot/backup_aws_secret_access_key" }}
RESTIC_REPOSITORY={{ key "secrets/email/dovecot/backup_restic_repository" }}
RESTIC_PASSWORD={{ key "secrets/email/dovecot/backup_restic_password" }}
EOH

         destination = "secrets/env_vars"
         env = true
      }

      resources {
        cpu = 500
        memory = 200
      }

      restart {
        attempts = 2
        interval = "30m"
        delay = "15s"
        mode = "fail"
      }
    }
  }

  group "backup-plume" {
    constraint {
      attribute = "${attr.unique.hostname}"
      operator = "="
      value = "digitale"
    }

    task "main" {
      driver = "docker"

      config {
        image = "restic/restic:0.12.1"
        entrypoint = [ "/bin/sh", "-c" ]
        args = [ "restic backup /plume && restic forget --keep-within 1m1d --keep-within-weekly 3m --keep-within-monthly 1y && restic prune --max-unused 50% --max-repack-size 2G && restic check" ]
        volumes = [
          "/mnt/ssd/plume/media:/plume"
        ]
      }

      template {
        data = <<EOH
AWS_ACCESS_KEY_ID={{ key "secrets/plume/backup_aws_access_key_id" }}
AWS_SECRET_ACCESS_KEY={{ key "secrets/plume/backup_aws_secret_access_key" }}
RESTIC_REPOSITORY={{ key "secrets/plume/backup_restic_repository" }}
RESTIC_PASSWORD={{ key "secrets/plume/backup_restic_password" }}
EOH

         destination = "secrets/env_vars"
         env = true
      }

      resources {
        cpu = 500
        memory = 200
      }

      restart {
        attempts = 2
        interval = "30m"
        delay = "15s"
        mode = "fail"
      }
    }
  }

  group "backup-consul" {
    task "consul-kv-export" {
      driver = "docker"

      lifecycle {      
        hook = "prestart"      
        sidecar = false    
      }

      config {
        image = "consul:1.11.2"
        network_mode = "host"
        entrypoint = [ "/bin/sh", "-c" ]
        args = [ "/bin/consul kv export > $NOMAD_ALLOC_DIR/consul.json" ]
      }

      env {
        CONSUL_HTTP_ADDR = "http://consul.service.2.cluster.deuxfleurs.fr:8500"
      }

      resources {
        cpu = 200
        memory = 200
      }

      restart {
        attempts = 2
        interval = "30m"
        delay = "15s"
        mode = "fail"
      }
    }

    task "restic-backup" {
      driver = "docker"

      config {
        image = "restic/restic:0.12.1"
        entrypoint = [ "/bin/sh", "-c" ]
        args = [ "restic backup $NOMAD_ALLOC_DIR/consul.json && restic forget --keep-within 1m1d --keep-within-weekly 3m --keep-within-monthly 1y && restic prune --max-unused 50% --max-repack-size 2G && restic check" ]
      }


      template {
        data = <<EOH
AWS_ACCESS_KEY_ID={{ key "secrets/backup/consul/backup_aws_access_key_id" }}
AWS_SECRET_ACCESS_KEY={{ key "secrets/backup/consul/backup_aws_secret_access_key" }}
RESTIC_REPOSITORY={{ key "secrets/backup/consul/backup_restic_repository" }}
RESTIC_PASSWORD={{ key "secrets/backup/consul/backup_restic_password" }}
EOH

         destination = "secrets/env_vars"
         env = true
      }

      resources {
        cpu = 200
        memory = 200
      }

      restart {
        attempts = 2
        interval = "30m"
        delay = "15s"
        mode = "fail"
      }
    }
  }
}
