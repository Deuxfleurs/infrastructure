deuxfleurs.fr
=============

**OBSOLETION NOTICE:** We are progressively migrating our stack to NixOS, to replace Ansible. Most of the files present in this repository are outdated or obsolete,
the current code for our infrastructure is at: <https://git.deuxfleurs.fr/Deuxfleurs/nixcfg>.

## I am lost, how this repo works?

To ease the development, we make the choice of a fully integrated environment

  1. `os` the base os for the cluster
      1. `build`: where you will build our OS image based on Debian that you will install on your server
      2. `config`: our Ansible recipes to configure and update your freshly installed server
  2. `apps` apps we deploy on the cluster
      1. `build`: our Docker files to build immutable images of our applications
      2. `integration`: Our Docker compose files to test locally how our built images interact together
      3. `config`: Files containing application configurations to be deployed on Consul Key Value Store
      4. `deployment`: Files containing application definitions to be deployed on Nomad Scheduler
  3. `op_guide`: Guides to explain you operations you can do cluster wide (like configuring postgres)


